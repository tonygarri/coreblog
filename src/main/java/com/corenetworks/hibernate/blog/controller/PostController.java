package com.corenetworks.hibernate.blog.controller;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.corenetworks.hibernate.blog.beans.CommentBean;
import com.corenetworks.hibernate.blog.beans.PostBean;
import com.corenetworks.hibernate.blog.beans.RegisterBean;
import com.corenetworks.hibernate.blog.dao.CommentDao;
import com.corenetworks.hibernate.blog.dao.PostDao;
import com.corenetworks.hibernate.blog.dao.UserDao;
import com.corenetworks.hibernate.blog.model.Comment;
import com.corenetworks.hibernate.blog.model.Post;
import com.corenetworks.hibernate.blog.model.User;



@Controller
public class PostController {
	@Autowired
	private PostDao postDao;
	
	@Autowired
	private CommentDao commentDao;
	
	@Autowired
	private HttpSession httpSession;

	@GetMapping(value = "/submit")
	public String showForm(Model modelo) {
		modelo.addAttribute("post", new PostBean());
		return "submit";
	}
	
	@PostMapping(value = "/submit/newpost")
	public String submit(@ModelAttribute("post") PostBean postBean , Model model) {
		//userDao.create(new User(r.getNombre(), r.getEmail(), r.getCiudad(), r.getPassword()));
         // crear un Post
		 // Obtener el autor desde la sesión
		 // Asignar el autor al post
		 // Hacer persistente el post
		Post post = new Post();
		post.setTitulo(postBean.getTitulo());
		post.setContenido(postBean.getContenido());
		post.setUrl(postBean.getUrl());
		
		User autor = (User) httpSession.getAttribute("userLoggedIn");
		post.setAutor(autor);
		postDao.create(post);
		autor.getPosts().add(post);
		
		return "redirect:/";
	}	
	
	@GetMapping(value = "/post/{id}")
	public String detail(@PathVariable("id")  long id,   Model modelo) {
		//modelo.addAttribute("post", new PostBean());
		// Comprobar si el Post existe.
		
		Post result = null;
		if ((result = postDao.getById(id)) != null) {
			modelo.addAttribute("post", result);
			modelo.addAttribute("commentForm", new CommentBean());
			return "postdetail";			
		} else
			return "redirect:/";
	}
	
	@PostMapping(value = "/submit/newComment")
	public String submitComment(@ModelAttribute("commentForm") CommentBean commentBean , Model model) {
		User autor = (User) httpSession.getAttribute("userLoggedIn");
		
		Comment comment = new Comment();
		comment.setUser(autor);
		
		Post post = postDao.getById(commentBean.getPost_id());
		comment.setPost(post);
		comment.setContenido(commentBean.getContenido());
		commentDao.create(comment);
		post.getComments().add(comment);
		autor.getComments().add(comment);
		
		
		return "redirect:/post/"+ commentBean.getPost_id();
	}	
	
	
}
