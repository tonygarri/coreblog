package com.corenetworks.hibernate.blog.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.corenetworks.hibernate.blog.model.Post;


@Repository
@Transactional
public class PostDao {
	
    @PersistenceContext
	private EntityManager entityManager;
    /*
     * Almacena el post en la base de datos
     */
    public void create(Post post) {
    	entityManager.persist(post);
    	return;
    }
    
    @SuppressWarnings("unchecked")
	public List<Post> getAll(){
    	return entityManager
    			.createQuery("select p from Post p")
    			.getResultList();
    }

	public Post getById(long id) {
		// TODO Auto-generated method stub
		return entityManager.find(Post.class, id);
	}
    
    
 
    
    
    
}