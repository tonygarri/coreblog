<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c"  %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix = "fn"  uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>   
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="author" content="Core Netowkrs Sevilla">
<meta name="description" content="Blog creado con Spring Boot, Spring MVC, JPA e Hibernate">

<title>Blog Curso Java Cloud</title>

<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"  rel="stylesheet">
<link href="assets/css/jumbotron-narrow.css"  rel="stylesheet">
</head>
<body>
  <div class="container">
    <div class="header clearfix">
	    <nav>
	       <ul  class="nav nav-pills pull-right">
		       <c:choose>
		         <c:when test="${not empty sessionScope.userLoggedIn }">
		            <jsp:include page="includes/menu_logged.jsp">
		            <jsp:param value="inicio" name="inicio"/>
		            <jsp:param name="usuario" value="${sessionScope.userLoggedIn.nombre }"/>
		            
		          </jsp:include>
		         </c:when>
		         <c:otherwise>
		          <jsp:include page="includes/menu.jsp">
		            <jsp:param value="inicio" name="inicio"/>
		          </jsp:include>
		         </c:otherwise> 
		       </c:choose>   
	       </ul>
	       
	    </nav>
	    <h3 class="text-muted">Blog Core</h3>
    
    </div>
    
 	    <div class="jumbotron">
          <h1>CoreBlog, el blog sobre Hibernate</h1>
          <p>Tutoriales, información y tips sobre Hibernate.</p>
          <c:if test="${empty sessionScope.userLoggedIn }">
               <p><a href="/signup"  role="button"  class="btn btn-lg btn-success ">Regístrate</a></p>
		  </c:if>
        </div>   
        
        
       <c:forEach items="${listaPost}" var="postItem">
       <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css"/>

  <div class="well">
      <div class="media">
      	<a class="pull-left" href="http://i.pravatar.cc">
    		<img class="media-object" src="http://i.pravatar.cc/180x180?u=${postItem.autor.email}"> 
  		</a>
  		<div class="media-body">
    		<h4 class="media-heading">${postItem.titulo}</h4>
          <p class="text-right">Autor ${postItem.autor.nombre}</p>
          <p> ${postItem.contenido}</p>
          <ul class="list-inline list-unstyled">
            <li><a class="btn btn-default" href="/post/${postItem.id}">Read More</a></li>
           <li>|</li>
           
  			<li><span><i class="glyphicon glyphicon-calendar"></i>  ${postItem.fecha} </span></li> 
            <li>|</li>
            <span><i class="glyphicon glyphicon-comment"> <a class="btn btn-default" href="/post/${postItem.id}">Comentarios</a></i> </span> 
            <li>|</li>
            <li>
            <!-- Use Font Awesome http://fortawesome.github.io/Font-Awesome/ -->
            <a href="https://www.facebook.com" target="_blank"><img alt="siguenos en facebook" height="32" src="http://2.bp.blogspot.com/-q_Tm1PpPfHo/UiXnJo5l-VI/AAAAAAAABzU/MKdrVYZjF0c/s1600/face.png" title="siguenos en facebook" width="32" /></a>
            <a href="https://accounts.google.com" target="_blank"><img alt="siguenos en Google+" height="32" src="http://1.bp.blogspot.com/-1W0m8yLdJtU/UidI-7j2BnI/AAAAAAAAB-Y/2wH5M_ZpXO8/s1600/plusone.png" title="siguenos en Google+" width="32" /></a>
            <a href="https://twitter.com" target="_blank"><img alt="siguenos en Twitter" height="32" src="http://3.bp.blogspot.com/-wlwaJJG-eOY/UiXnHS2jLsI/AAAAAAAAByQ/I2tLyZDLNL4/s1600/Twitter+NEW.png" title="siguenos en Twitter" width="32" /></a>
            <a href="https://www.blog.google/" target="_blank"><img alt="sígueme en Blogger" height="32" src="http://1.bp.blogspot.com/-_NWymh6-9I4/UiXnEcZ1UMI/AAAAAAAABw4/UkFXkztSeOY/s1600/Google+Blogger.png" title="sígueme en Blogger" width="32" /></a>
            
              
              
            </li>
			</ul>
       </div>
    </div>
  </div>

        
       </c:forEach>
  	
  	
  	
  
  	</div>
  
  <script src="webjars/jquery/3.1.1/jquery.min.js"></script> 
  <script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
   <script src="assets/js/jquery.dotdotdot.min.js"> </script>
    <script>
 jQuery(document).ready(function() {

   $(".dotdotdot").dotdotdot({
      height:150
   });

}); 

 </script>

  
</body>
</html>